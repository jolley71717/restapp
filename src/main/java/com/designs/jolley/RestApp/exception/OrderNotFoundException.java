package com.designs.jolley.RestApp.exception;

/**
 * Created by Luke Dutton on 10/25/18
 *
 * @author Luke Dutton
 * @since 10/25/18
 */
public class OrderNotFoundException extends RuntimeException {

    public OrderNotFoundException(Long id){
        super("Could not find order " + id);
    }
}
