package com.designs.jolley.RestApp.model.repository;

import com.designs.jolley.RestApp.model.pojo.Order;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Luke Dutton on 10/26/18
 *
 * @author Luke Dutton
 * @since 10/26/18
 */
public interface IOrderRepository extends JpaRepository<Order, Long> {
}
