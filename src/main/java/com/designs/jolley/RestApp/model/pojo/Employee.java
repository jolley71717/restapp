package com.designs.jolley.RestApp.model.pojo;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Luke Dutton on 10/25/18
 *
 * @author Luke Dutton
 * @since 10/25/18
 */
@Data
@Entity
public class Employee {

    private @Id @GeneratedValue Long id;
    private String firstName;
    private String lastName;
    private String role;

    public Employee(String firstName, String lastName, String role) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.role = role;
    }

    public Employee(String name, String role){
        setName(name);
        this.role = role;
    }

    public String getName() {
        return this.firstName + " " + this.lastName;
    }

    public void setName(String name) {
        String[] parts =name.split(" ");
        this.firstName = parts[0];
        this.lastName = parts[1];
    }
}
