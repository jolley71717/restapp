package com.designs.jolley.RestApp.model.repository;

import com.designs.jolley.RestApp.model.pojo.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Luke Dutton on 10/25/18
 *
 * @author Luke Dutton
 * @since 10/25/18
 */
@Repository
public interface IEmployeeRepository extends JpaRepository<Employee, Long> {

}
