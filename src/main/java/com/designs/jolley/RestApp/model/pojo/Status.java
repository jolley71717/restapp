package com.designs.jolley.RestApp.model.pojo;

/**
 * Created by Luke Dutton on 10/26/18
 *
 * @author Luke Dutton
 * @since 10/26/18
 */
public enum Status {
    IN_PROGRESS,
    COMPLETED,
    CANCELLED
}
