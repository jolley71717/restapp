package com.designs.jolley.RestApp;

import com.designs.jolley.RestApp.model.pojo.Employee;
import com.designs.jolley.RestApp.model.pojo.Order;
import com.designs.jolley.RestApp.model.pojo.Status;
import com.designs.jolley.RestApp.model.repository.IEmployeeRepository;
import com.designs.jolley.RestApp.model.repository.IOrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Luke Dutton on 10/25/18
 *
 * @author Luke Dutton
 * @since 10/25/18
 */
@Configuration
@Slf4j
public class LoadDatabase {

    @Bean
    public CommandLineRunner initDatabase(IEmployeeRepository repository, IOrderRepository orderRepository){
        return args -> {

            Employee emp1 = repository.save(new Employee("Bilbo Baggins", "burglar"));
            Employee emp2 = repository.save(new Employee("Bilbo Baggins", "burglar"));
            Employee emp3 = repository.save(new Employee("Frodo Baggins", "thief"));

            log.info("Preloading " + emp1);
            log.info("Preloading " + emp2);
            log.info("Preloading " + emp3);

            orderRepository.save(new Order("MacBook Pro", Status.COMPLETED, emp1.getId()));
            orderRepository.save(new Order("iPhone", Status.IN_PROGRESS, emp2.getId()));
            orderRepository.save(new Order("Magic Mouse", Status.IN_PROGRESS, emp3.getId()));


            repository.findAll().forEach( employee -> log.info("Preloaded " + employee));
            orderRepository.findAll().forEach(order -> {
                log.info("Preloaded " + order);
            });
        };
    }
}
