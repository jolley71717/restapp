package com.designs.jolley.RestApp.resourceassembler;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;


import com.designs.jolley.RestApp.controller.EmployeeController;
import com.designs.jolley.RestApp.model.pojo.Employee;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

/**
 * Created by Luke Dutton on 10/25/18
 *
 * @author Luke Dutton
 * @since 10/25/18
 */
@Component
public class EmployeeResourceAssembler implements ResourceAssembler<Employee, Resource<Employee>> {

    @Override
    public Resource<Employee> toResource(Employee employee) {

        return new Resource<>(employee,
                linkTo(methodOn(EmployeeController.class).one(employee.getId())).withSelfRel(),
                linkTo(methodOn(EmployeeController.class).all()).withRel("employees"));
    }
}
