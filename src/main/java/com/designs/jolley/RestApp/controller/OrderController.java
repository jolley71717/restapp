package com.designs.jolley.RestApp.controller;

import com.designs.jolley.RestApp.exception.OrderNotFoundException;
import com.designs.jolley.RestApp.model.pojo.Order;
import com.designs.jolley.RestApp.model.pojo.Status;
import com.designs.jolley.RestApp.model.repository.IOrderRepository;
import com.designs.jolley.RestApp.resourceassembler.OrderResourceAssembler;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.VndErrors;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by Luke Dutton on 10/26/18
 *
 * @author Luke Dutton
 * @since 10/26/18
 */
@RestController
public class OrderController {

    private final IOrderRepository orderRepository;
    private final OrderResourceAssembler orderResourceAssembler;

    public OrderController(IOrderRepository orderRepository, OrderResourceAssembler orderResourceAssembler){
        this.orderRepository = orderRepository;
        this.orderResourceAssembler = orderResourceAssembler;
    }

    @GetMapping("/orders")
    public ResponseEntity<Resources<Resource<Order>>> all() {
        List<Resource<Order>> orders = orderRepository.findAll().stream()
                .map(orderResourceAssembler::toResource)
                .collect(Collectors.toList());

        return ResponseEntity
                    .ok()
                .body(new Resources<>(orders,
                    linkTo(methodOn(OrderController.class).all()).withSelfRel())) ;
    }

    @GetMapping("/orders/{id}")
    public Resource<Order> one(@PathVariable Long id) {
        return orderResourceAssembler.toResource(
                orderRepository.findById(id)
                        .orElseThrow(() -> new OrderNotFoundException(id)));
    }

    @PostMapping("/orders")
    public ResponseEntity<Resource<Order>> newOrder(@RequestBody Order order) {

        order.setStatus(Status.IN_PROGRESS);
        Order newOrder = orderRepository.save(order);

        return ResponseEntity
                .created(linkTo(methodOn(OrderController.class).one(newOrder.getId())).toUri())
                .body(orderResourceAssembler.toResource(newOrder));
    }

    @PutMapping("/orders/{id}/complete")
    public ResponseEntity<ResourceSupport> complete(@PathVariable Long id) {

        Order order = orderRepository.findById(id).orElseThrow(() -> new OrderNotFoundException(id));

        if (order.getStatus() == Status.IN_PROGRESS) {
            order.setStatus(Status.COMPLETED);
            return ResponseEntity.ok(orderResourceAssembler.toResource(orderRepository.save(order)));
        }

        return ResponseEntity
                .status(HttpStatus.METHOD_NOT_ALLOWED)
                .body(new VndErrors.VndError("Method not allowed", "You can't complete an order that is in the " + order.getStatus() + " status"));
    }

    @DeleteMapping("/orders/{id}/cancel")
    public ResponseEntity<ResourceSupport> cancel(@PathVariable Long id) {

        Order order = orderRepository.findById(id).orElseThrow(() -> new OrderNotFoundException(id));

        if (order.getStatus() == Status.IN_PROGRESS) {
            order.setStatus(Status.CANCELLED);
            return ResponseEntity.ok(orderResourceAssembler.toResource(orderRepository.save(order)));
        }

        return ResponseEntity
                .status(HttpStatus.METHOD_NOT_ALLOWED)
                .body(new VndErrors.VndError("Method not allowed", "You can't cancel an order that is in the " + order.getStatus() + " status"));
    }
}

