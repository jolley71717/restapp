let repoUrl = 'http://localhost:8080';
angular.factory('Employee', ['$http', function($http) {
    function Employee(empData) {
        if (empData) {
            this.setData(empData);
        }
        // Some other initializations related to book
    };
    Employee.prototype = {
        setData: function(empData) {
            angular.extend(this, empData);
        },
        load: function(empID) {
            var scope = this;
            $http.get(repoUrl + '/employees/' + empID).success(function(empData) {
                scope.setData(empData);
            });
        },
        delete: function() {
            $http.delete(repoUrl + '/employees/' + empID );
        },
        update: function() {
            $http.put(repoUrl + '/employees/' + empID, this);
        }
    };
    return Employee;
}]);