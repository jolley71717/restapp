let repoUrl = 'http://localhost:8080';
angular.factory('Order', ['$http', function($http) {
    function Order(orderData) {
        if (orderData) {
            this.setData(orderData);
        }
        // Some other initializations related to book
    };
    Order.prototype = {
        setData: function(orderData) {
            angular.extend(this, orderData);
        },
        load: function(orderId) {
            var scope = this;
            $http.get(repoUrl + '/orders/' + orderId).success(function(orderData) {
                scope.setData(orderData);
            });
        },
        delete: function() {
            $http.delete(repoUrl + '/orders/' + orderId + '/cancel');
        },
        update: function() {
            $http.put(repoUrl + '/orders/' + orderId + '/complete', this);
        }
    };
    return Order;
}]);