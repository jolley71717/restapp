'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'myApp.view1',
  'myApp.view2',
  'myApp.version'
]).controller('Hello', function($scope, $http) {
    $http.get('http://rest-service.guides.spring.io/greeting')
        .then(function (response) {
            $scope.greeting = response.data;
        });

})
    .config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider.otherwise({redirectTo: '/view1'});
}]);


